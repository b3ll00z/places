# Places

This project aims to practising on native devices features in React Native development.
The app allows a user to save favourite places by saving the position where he/her is located.
In particular, a favourite place is described by a short description, a photo taken by camera and position
marked on Google map.

The exercise follows the lessons based by this greate RN crash [course](https://github.com/academind/react-native-practical-guide-code/tree/12-native-features).
